#!/usr/bin/env bash

addPersistentEnvironmentVariable() {
	local name=$1; value=$2
	echo "export $name=\"$value\"" >> ~/.zshrc
	echo "export $name=\"$value\"" >> ~/.bashrc 
}

generateSSHKey() {
  local name=$1; url=$2
  echo "================================================================================"
  echo "Creating Public/Private Key for git ($url)"
  ssh-keygen -t ed25519 -C "Key for $name ($url) created by $USER on $HOSTNAME" -b 4096 -f "$HOME/.ssh/$name" > /dev/null
  cat ~/.ssh/"$name.pub"
  echo "================================================================================"
  read -rp "Paste this key to your account on $name. Press any Keyboard Key to continue..."
  echo ""
}

flatpaks=(
	"com.axosoft.GitKraken"
	"com.discordapp.Discord"
	"com.jetbrains.PyCharm-Community"
	"com.jetbrains.Rider"
	"com.microsoft.Teams"
	"com.obsproject.Studio"
	"com.valvesoftware.Steam"
	"im.riot.Riot"
	"io.dbeaver.DBeaverCommunity"
	"org.gnome.seahorse.Application"
	"org.jitsi.jitsi-meet"
	"org.octave.Octave"
	"org.signal.Signal"
	"com.visualstudio.code"
	"rest.insomnia.Insomnia"
	"com.jetbrains.CLion"
	"com.jetbrains.IntelliJ-IDEA-Ultimate"
	"com.google.AndroidStudio"
	"org.ghidra_sre.Ghidra"
	"com.jgraph.drawio.desktop"
	"app.resp.RESP"
	"com.github.reds.LogisimEvolution"
)

flatpak_sdks=(
	"vala"
	"node16"
	"golang"
	"dotnet"
	"dotnet6"
	"openjdk"
	"rust-stable"
)

echo "➜ Installing Flathub Repository"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "➜ Installing Flatpaks"
for flatpak in "${flatpaks[@]}"; do
  echo -n "    Installing $flatpak ... "
  if(flatpak install -y --noninteractive --or-update  "$flatpak" > /dev/null)
    then echo -e " \e[32mOK\e[0m"; 
    else echo -e " \e[31mFAILED\e[0m"; 
  fi   
done

echo "➜ Installing Flatpak SDK Extensions"
for sdk in "${flatpak_sdks[@]}"; do
  echo -n "    Installing org.freedesktop.Sdk.Extension.$sdk ... "
  if(flatpak install -y --or-update  "org.freedesktop.Sdk.Extension.$sdk")
	then echo -e " \e[32mOK\e[0m"; 
    else echo -e " \e[31mFAILED\e[0m"; 
  fi  
done

addPersistentEnvironmentVariable "FLATPAK_ENABLE_SDK_EXT" "$(echo "${flatpak_sdks[@]}" | tr ' ' ',')"

echo "➜ Creating User Directory Structures"
mkdir -p /home/rare/.auth > /dev/null
mkdir -p /home/rare/Workspace > /dev/null
mkdir -p /home/rare/Playground > /dev/null

echo "➜ Configuring GNOME"
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 1800
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface show-battery-percentage true

echo "➜ Installing Oh My ZSH"
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "➜ Configuring SSH Keys & git"
generateSSHKey "gitlab" "https://gitlab.com"
generateSSHKey "github" "https://github.com"
generateSSHKey "framagit" "https://framagit.org"
generateSSHKey "codeberg" "https://codeberg.org"
generateSSHKey "bitbucket" "https://bitbucket.org"
generateSSHKey "fhjoanneum" "https://git-iit.fh-joanneum.at"

git config --global user.name "Raffael Rehberger"
git config --global user.email "raffael@rtrace.io"

echo "➜ Configuring JAVA and Android"
addPersistentEnvironmentVariable "JAVA_HOME" "$(readlink -f /usr/bin/java | sed s:bin/java::)"
addPersistentEnvironmentVariable "ANDROID_SDK_ROOT" "/home/$(whoami)/Android/Sdk/"
addPersistentEnvironmentVariable "PATH" "$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/:$ANDROID_SDK_ROOT/tools/bin/:$ANDROID_SDK_ROOT/platform-tools/:$ANDROID_SDK_ROOT/build-tools/32.0.0/"
