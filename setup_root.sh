#!/usr/bin/env bash

dnfpackages=(
	"git"
	"nano"
	"wget"
	"curl"
	"htop"
	"tcpdump"
	"ca-certificates"
	"ffmpeg"
	"cockpit"
	"p7zip"
	"p7zip-plugins"
	"podman"
	"zsh"
	"powerline-fonts"
	"wine"
	"wine-*"
	"wireguard-tools"
	"zip"
	"unzip"
	"bind-utils"
	"NetworkManager-openvpn"
	"NetworkManager-openvpn-gnome"
	"keepassxc"
	"tilix"
	"VirtualBox"
	"java-latest-openjdk-devel"
	"java-latest-openjdk"
	"libreoffice-langpack-de"
	"gns3-server"
	"gns3-gui"
	"numix-icon-theme-circle"
	"gnome-tweaks"
)

dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

echo "➜ Updating System"
dnf update -y > /dev/null

echo "➜ Installing dnf Packages"
for dnfpackage in "${dnfpackages[@]}"; do
	echo -n "Installing $dnfpackage ..."
  if (dnf install -y "$dnfpackage" > /dev/null)
    then echo -e " \e[32mOK\e[0m";
    else echo -e " \e[31mFAILED\e[0m";
  fi
done

echo "➜ Installing Media Codecs"
dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
dnf install -y lame\* --exclude=lame-devel
dnf group upgrade --with-optional Multimedia
